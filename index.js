import { LoadingManager, TextureLoader } from 'three';

class MultiLoader {
    constructor( options = {} ) {
        this.logs = typeof options.logs == 'boolean' ? options.logs : false;
        this.progressBar = typeof options.progressBar == 'boolean' ? options.progressBar : true;
        this.texturesFlipY = typeof options.texturesFlipY == 'boolean' ? options.texturesFlipY : true;
        this.progress = 0;
        this.onProgress = false;
        this.init = false;
        this.domElements = {};
        this.loadingManager = new LoadingManager();
        this.handleLoadingManager();
        this.textureLoader = new TextureLoader( this.loadingManager );

        if ( options.GLTFLoader ) {
            this.gltfLoader = new options.GLTFLoader( this.loadingManager );
        }

        if ( options.CubeTextureLoader ) {
            this.cubeTextureLoader = new options.CubeTextureLoader( this.loadingManager );
        }

        if ( options.KTX2Loader ) {
            if ( !options.renderer ) {
                throw new Error('[MultiLoader] A renderer must be provided to load KTX2 compressed textures. https://www.npmjs.com/package/@thibka/three-multiloader'); 
            }
            let basisTranscoderPath = options.basisTranscoderPath || 'node_modules/three/examples/jsm/libs/basis/';
            this.KTX2Loader = new options.KTX2Loader( this.loadingManager );
            this.KTX2Loader.detectSupport( options.renderer );
            this.KTX2Loader.setTranscoderPath( basisTranscoderPath );
        }

        if ( options.DRACOLoader ) {
            let DRACODecoderPath = options.DRACODecoderPath || 'node_modules/three/examples/jsm/libs/draco/';
            this.DRACOLoader = new options.DRACOLoader( this.loadingManager );
            this.DRACOLoader.setDecoderPath( DRACODecoderPath );
            this.DRACOLoader.setDecoderConfig( { type: 'js' } );
        }

        if ( options.RGBELoader ) {
            this.RGBELoader = new options.RGBELoader( this.loadingManager );
        }

        if ( options.lottieLoader ) { 
            this.lottieLoader = new options.lottieLoader( this.loadingManager );
            this.lottieLoader.setQuality( options.lottieLoaderQuality || 1 );
        }

        if ( options.OBJLoader ) {
            this.OBJLoader = new options.OBJLoader( this.loadingManager );
        }

        this.images = [];
        this.textures = [];
        this.cubeTextures = [];
        this.models = [];
        this.videos = [];        
        this.onloadCallbacks = [];
        this.loaded = false;
        this.forceLoadIntervals = [];
        this.onLoaded = {
            add: this._onLoadedAdd.bind(this)
        };
    }

    handleLoadingManager() {
        this.loadingManager.onProgress = ( url, itemsLoaded, itemsTotal ) => {
            if (this.logs) {
                console.log( 'Loading file: ' + url + '.\nLoaded ' + itemsLoaded + ' of ' + itemsTotal + ' files.' );
            }

            this.progress = itemsLoaded / itemsTotal;
            
            if (this.progressBar && this.init)  {
                let completion = 100 - this.progress * 100;
                this.domElements.loaderCompletion.style.transform = 'translateX(-' + completion + '%)';     
            }

            if (typeof this.onProgress == 'function') {
                this.onProgress(this.progress);
            }
        };

        this.loadingManager.onError = function ( url ) {
            console.warn( 'There was an error loading ' + String(url) );
        };

        this.loadingManager.onLoad = () => {
            if (this.progressBar) {
                this.domElements.loaderBar.style.opacity = 0;
                setTimeout(() => {
                    this.domElements.loader.style.opacity = 0;
                    setTimeout(() => {
                        this.domElements.loader.style.display = 'none';
                    }, 600);
                }, 1000);
            }

            this.onloadCallbacks.forEach(callback => {
                callback();
            });

            this.loaded = true;
        };
    }

    _onLoadedAdd(callback) {
        if (typeof callback != 'function') throw new Error('[MultiLoader] onLoaded() parameter must be a function.' );
        if (!this.loaded) {
            this.onloadCallbacks.push(callback);
        } else callback();

    }

    createProgressBar() {
        let styles = /* css */`
            .loader {
                position: absolute;
                z-index: 1000;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: black;
                display: flex;
                justify-content: center;
                align-items: center;
                transition: opacity .5s linear;
            }
            
            .loader__bar {
                width: 75%;
                height: 2px;
                background: #333333;
                overflow: hidden;
                transition: opacity .5s linear;
            }
            
            .loader__completion {
                width: 100%;
                height: 100%;
                background: white;
                transform: translateX(-100%);
                transition: transform .2s ease;
            }
        `;

        let styleTag = document.createElement('style');
        styleTag.id = 'three-multiloader-progressbar';
        styleTag.innerHTML = styles;
        document.head.appendChild(styleTag);
    
        var loaderDiv = document.createElement('div');
        loaderDiv.id = 'loader';
        loaderDiv.classList.add('loader');
        document.body.appendChild(loaderDiv);
    
        var loaderBar = document.createElement('div');
        loaderBar.id = 'loader__bar';
        loaderBar.classList.add('loader__bar');
        loaderDiv.appendChild(loaderBar);
    
        var loaderCompletion = document.createElement('div');
        loaderCompletion.id = 'loader__completion';
        loaderCompletion.classList.add('loader__completion');
        loaderBar.appendChild(loaderCompletion);
    
        this.domElements = {loader: loaderDiv, loaderBar, loaderCompletion};
    }

    async loadKTX2(path) {
        return await this.KTX2Loader.loadAsync( path );
    }

    load(assets) {
        this.init = true;

        if (this.progressBar) {
            this.createProgressBar();
        }

        if (!assets || assets.length == 0) {
            // if there's nothing to load, triggers end of loading manager
            this.loadingManager.itemStart();
            this.loadingManager.itemEnd();
        }        
            
        assets.forEach(asset => {
            var assetName;
            if ( asset.alias )
                assetName = asset.alias;
            else {
                if (typeof asset.path == "string")
                    assetName = asset.path;
                else 
                    throw new Error('[MultiLoader] An alias must be provided for this asset: ' + asset.path);
            }
            if (asset.type == 'texture') {
                var extension = asset.path.split('.').pop();
                
                // KTX2 textures
                if ( extension == 'ktx2' ) {
                    if ( this.KTX2Loader ) {
                        this.loadingManager.itemStart( asset.path );
                        this.textures[assetName] = null;
                        this.textures[assetName] = this.loadKTX2(asset.path);
                    }
                    else {
                        throw new Error('[MultiLoader] KTX2Loader is not defined. https://www.npmjs.com/package/@thibka/three-multiloader');
                    }
                }

                // hdr textures
                else if ( extension == 'hdr' ) {
                    if ( this.RGBELoader ) {
                        this.textures[assetName] = this.RGBELoader.load( asset.path );
                    }
                    else {
                        throw new Error('[MultiLoader] RGBELoader is needed to load HDR textures. https://www.npmjs.com/package/@thibka/three-multiloader');
                    }
                }

                // lottie textures
                else if ( extension == 'json' ) {
                    if ( this.lottieLoader ) {
                        this.textures[assetName] = this.lottieLoader.load( asset.path );
                    }
                    else {
                        throw new Error('[MultiLoader] LottieLoader is needed to load Lottie textures. https://www.npmjs.com/package/@thibka/three-multiloader');
                    }
                }

                // regular textures
                else { 
                    this.textures[assetName] = this.textureLoader.load(asset.path);
                }
                
                if (this.textures[assetName]) {
                    if (!this.textures[assetName].then) {
                        // Most textures are created immediately,
                        // so we can apply their options right away
                        this.applyTextureOptions(asset, assetName);
                    } else {
                        // With KTX2 texture, we need to wait for
                        // a promise to resolve
                        this.textures[assetName].then((texture) => {
                            this.textures[assetName] = texture;
                            this.applyTextureOptions(asset, assetName);
                            this.loadingManager.itemEnd( asset.path );
                        }) 
                    }
                }
            }
            else if (asset.type == 'image') {
                let image = new Image();
                this.loadingManager.itemStart( asset.path );
                image.onload = () => {
                    this.images[assetName] = image;
                    this.loadingManager.itemEnd( asset.path );
                }
                image.src = asset.path;
            }
            else if (asset.type == 'cube-texture') {            
                if (typeof asset.path != 'object') {
                    throw new Error('[MultiLoader] cube-texture paths must be arrays');                
                } 
                else {
                    if ( this.cubeTextureLoader ) {
                        this.cubeTextures[assetName] = this.cubeTextureLoader.load(asset.path);
                    }
                    else {
                        throw new Error('[MultiLoader] CubeTextureLoader is not defined. https://www.npmjs.com/package/@thibka/three-multiloader');
                    }
                }
            }
            else if (asset.type == 'gltf' || asset.type == 'gltf-draco') {
                if (asset.type == 'gltf-draco' && !this.gltf_draco_loader_set) {
                    this.gltfLoader.setDRACOLoader( this.DRACOLoader );
                    this.gltf_draco_loader_set = true;
                }
                this.gltfLoader.load( asset.path,
                    gltf => {
                        this.models[assetName] = gltf;
                    },
                    xhr => {
                        //console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
                    },
                    error => {
                        console.log(asset);
                        console.error(error);
                    }
                );
            }
            else if (asset.type == 'obj') {                
                if ( this.OBJLoader ) {
                    this.OBJLoader.load( asset.path,
                        obj => {                            
                            this.models[assetName] = obj;
                        },
                        xhr => {
                            //console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
                        },
                        error => {
                            console.log(asset);
                            console.error(error);
                        }
                    );
                }
                else {
                    throw new Error('[MultiLoader] OBJLoader is not defined. https://www.npmjs.com/package/@thibka/three-multiloader');
                }
            }
            else if (asset.type == 'draco' || asset.type == 'drc') {
                if ( this.DRACOLoader ) {
                    this.loadingManager.itemStart( asset.path );
                    this.DRACOLoader.load( asset.path,
                        geometry => {
                            geometry.computeVertexNormals();
                            
                            this.models[assetName] = geometry;

                            this.DRACOLoader.dispose();
                            this.loadingManager.itemEnd( asset.path );
                        },
                        xhr => {
                            //console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
                        },
                        error => {
                            console.log(asset);
                            console.error(error);
                        }
                    );
                }
                else {
                    throw new Error('[MultiLoader] DRACOLoader is not defined. https://www.npmjs.com/package/@thibka/three-multiloader');
                }
            }
            else if (asset.type == 'video') { 
                this.loadingManager.itemStart( asset.path );
                this.videos.length += 1;
                let video = document.createElement('video');
                let loadEvent = asset.loadEvent != undefined ? asset.loadEvent : 'loadedmetadata';

                video.setAttribute('crossOrigin', 'anonymous');
                video.setAttribute('data-load-event', loadEvent);
                video.setAttribute('data-alias', assetName);
                video.addEventListener(loadEvent, this.handleLoadedVideo.bind(this));
                
                video.muted = true;
                video.loop = true;
                video.src = asset.path;
                video.load();
            }
        });    
    }

    applyTextureOptions(asset, assetName) {
        let texture = this.textures[assetName];
        if (asset.colorSpace) {
            texture.colorSpace = asset.colorSpace;
        }
        
        if (typeof asset.flipY == 'boolean') {
            texture.flipY = asset.flipY;
        } else {
            texture.flipY = this.texturesFlipY;
        }
    }

    handleLoadedVideo(ev) {
        let loadedVideo;        
        if (ev.path != undefined && ev.path[0]) loadedVideo = ev.path[0]; // Chrome 
        else loadedVideo = ev.target; // Firefox, Safari
        let loadEvent = loadedVideo.dataset.loadEvent;
        let alias = loadedVideo.dataset.alias;
        loadedVideo.removeEventListener(loadEvent, this.handleLoadedVideo.bind(this));
        clearInterval(this.forceLoadIntervals[loadedVideo.src]);                    
        this.videos[alias] = loadedVideo;
        this.loadingManager.itemEnd( loadedVideo.src );
    };
}

export default MultiLoader;