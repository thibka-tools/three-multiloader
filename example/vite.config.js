// https://vitejs.dev/config/
import { viteStaticCopy } from 'vite-plugin-static-copy';

export default {
  base: '',
  plugins: [
    viteStaticCopy({
      targets: [
        {
          src: './node_modules/three/examples/jsm/libs/draco/draco_decoder.js',
          dest: 'draco'
        },
        {
          src: './node_modules/three/examples/jsm/libs/basis',
          dest: ''
        }
      ]
    })
  ]
}