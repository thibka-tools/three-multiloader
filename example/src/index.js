import './styles/styles.css';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import MultiLoader from '../../index';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js';
import { LottieLoader } from 'three/examples/jsm/loaders/LottieLoader.js';
import { KTX2Loader } from 'three/examples/jsm/loaders/KTX2Loader.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';

const canvas = document.getElementById('canvas');
const renderer = new THREE.WebGLRenderer({ canvas });
renderer.setSize(window.innerWidth, window.innerHeight);

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.z = 5;

const controls = new OrbitControls(camera, canvas);

// render loop
function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
    controls.update();
}
animate();

window.addEventListener('resize', () => {
    renderer.setSize(window.innerWidth, window.innerHeight);
    camera.aspect = window.innerWidth / window.innerHeight;    
    camera.updateProjectionMatrix();
});

let suzanne;

const loader1 = new MultiLoader({ 
    progressBar: true,
    logs: true,
    
    GLTFLoader,
    DRACOLoader,
    DRACODecoderPath: 'draco/',
});

const loader2 = new MultiLoader({ 
    progressBar: false, 
    logs: true, 
    KTX2Loader, 
    basisTranscoderPath: 'basis/',
    renderer,
    RGBELoader
});

const loader3 = new MultiLoader({ 
    progressBar: false, 
    logs: true, 
    lottieLoader: LottieLoader,
    lottieLoaderQuality: 2
});

const loader4 = new MultiLoader({ 
    progressBar: false, 
    OBJLoader
});

loader1.onLoaded.add(() => {
    addGLTFModel.call(this);
    addDRCModel.call(this);
    addGLTFDracoModel.call(this);
    addLight.call(this);
    addObjModel.call(this);

    setTimeout(() => {
        loadTextures();
    }, 1000);
});

loader1.load([
    { alias: 'suzanne', type: 'gltf', path: 'models/suzanne.glb' },
    { alias: 'randomImage', type: 'image', path: 'textures/random.jpg' },
    { alias: 'dracoModel', type: 'drc', path: 'models/bunny.drc' },
    { alias: 'icosphere-draco', type: 'gltf-draco', path: 'models/icosphere-draco.glb' },
]);

function addGLTFModel() {
    let gltf = loader1.models['suzanne'];

    gltf.scene.traverse(function (child) {
        if (child.isMesh) {
            suzanne = child;
            child.material.metalness = .5;
            child.material.roughness = .5;
            child.scale.set(.5, .5, .5);
        }
    });

    scene.add(gltf.scene);
}

function addDRCModel() {
    let geometry = loader1.models['dracoModel'];

    let dracoMesh = new THREE.Mesh(
        geometry,
        new THREE.MeshStandardMaterial({
            metalness: .5,
            roughness: .5
        })
    );

    dracoMesh.position.set(1.5, -.9, -.5);
    dracoMesh.scale.set(8, 8, 8);

    scene.add( dracoMesh );
}

function addGLTFDracoModel() {
    let gltf = loader1.models['icosphere-draco'];

    gltf.scene.traverse(function (child) {
        if (child.isMesh) {
            child.material.color = new THREE.Color(0x336699);
            child.material.metalness = .5;
            child.material.roughness = .5;
            child.position.x = -.5;
            child.scale.set(.5, .5, .5);
        }
    });

    gltf.scene.position.x = -.8;

    scene.add(gltf.scene);
}

function addLight() {
    var light = new THREE.PointLight(0xffffff, 1);
    light.position.set(10, 10, 30);
    scene.add(light);

    scene.add(new THREE.AmbientLight(0xffffff));
}

function loadTextures() {
    loader2.onLoaded.add(() => {
        suzanne.material.map = loader2.textures['ktx2_texture'];
        suzanne.material.needsUpdate = true;

        loader2.textures['hdr'].mapping = THREE.EquirectangularReflectionMapping;
        scene.background = loader2.textures['hdr'];

        console.log('Loader 2 has finished loading.');
    });

    loader2.load([
        //{ alias: 'metal', type: 'texture', path: 'textures/metal.jpg' },
        { type: 'texture', path: 'textures/metal.ktx2', alias: 'ktx2_texture' },
        { type: 'texture', path: 'textures/belfast_sunset_puresky_2k.hdr', colorSpace: THREE.SRGBColorSpace, alias: 'hdr' }
    ]);

    loader3.onLoaded.add(() => {
        let cube = new THREE.Mesh(
            new THREE.BoxGeometry(1, 1, 1),
            new THREE.MeshBasicMaterial({
                map: loader3.textures['lottie']
            })
        );
        cube.position.y = 1.5;
        scene.add(cube);
    });

    loader3.load([
        { type: 'texture', path: 'textures/lottie-logo-animation.json', alias: 'lottie' }
    ]);
}

function addObjModel() {    
    loader4.onLoaded.add(() => {        
        const torus = loader4.models['torus'];
        torus.position.y = -1.5;

        scene.add( torus );
    });

    loader4.load([
        { type: 'obj', path: 'models/torus.obj', alias: 'torus' }
    ]);
}