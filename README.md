<h1 align="center">three-multiloader</h1>

<p align="center">Three.js loading manager for textures, basis textures, cube textures, images, videos, and GLTF files.</p>

## Table of Contents
- [Install](#install)
- [Basic usage](#basics)
- [Options](#options)
    - [progressBar](#progressbar)
    - [logs](#logs)
    - [texturesFlipY](#texturesflipy)
- [Custom progress animation](#custom-progress-animation)
- [Loading examples](#loading-examples)
    - [Texture](#texture)
    - [HDR texture](#hdr-texture)
    - [Image](#image)
    - [GLTF](#gltf)
    - [Video](#video)
    - [CubeTexture](#cubetexture)
    - [KTX2 compressed texture](#ktx2-compressed-texture)
    - [DRACO compressed GLTF file](#draco-compressed-GLTF-file)
    - [.drc file](#drc-file)
    - [Lottie texture](#lottie-texture)
    - [OBJ](#obj)


## Install

```bash
npm i @thibka/three-multiloader
```

## Basic usage

```javascript
import MultiLoader from '@thibka/three-multiloader'

let loader = new MultiLoader()

loader.onLoaded.add(() => {
    console.log('Loading completed!')
    console.log(loader.textures['cat'])
});

loader.load([
    { alias: 'cat', type: 'texture', path: 'path/to/cute-cat.jpg' }
])
```


## Options

Options are passed in an object during instanciation
```javascript
let loader = new MultiLoader({ 
    progressBar: false,
    logs: true,
    texturesFlipY: true
})
```

### progressBar

Defines if the default full-screen progress bar should be automatically displayed.  
Default is `true`.

### logs

If true, outputs the loading progress in the console.  
Default is `false`.

### texturesFlipY

Defines if the textures should be flipped along the vertical axis when uploaded to the GPU.  
Default is `false`.


# Custom progress animation

To create a custom loading animation, first, disable the default progress bar.

```js
let loader = new MultiLoader({ progressBar: false })
```

A custom loading function can then be defined using the `onProgress` callback, before calling the `load()` method.
```javascript
loader.onProgress = progress => {
    console.log(progress); // Will display 0 to 1 loading progress
}
```
Additionally, you can access the loading completion this way:
```javascript
loader.progress; // returns a number between 0 and 1.
```

# Loading examples

## Texture

```javascript
import MultiLoader from '@thibka/three-multiloader'

let loader = new MultiLoader()

loader.onLoaded.add(() => {
    console.log(loader.textures['cat'])
    console.log(loader.textures['path/to/funny-dog.png'])
})

loader.load([
    // encoding can be provided right away, default is THREE.LinearEncoding
    { type: 'texture', path: 'path/to/cute-cat.jpg', alias: 'cat', encoding: THREE.sRGBEncoding },

    // if no alias is used, the asset name will be its path
    { type: 'texture', path: 'path/to/funny-dog.png' },     
])
```

## HDR texture

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js';

const loader = new MultiLoader({ 
    RGBELoader
});

loader.onLoaded.add(() => {
    console.log(loader.textures['hdr'])
});

loader.load([
    { alias: 'hdr', type: 'texture', path: 'path/toe/image.hdr' }
]);
```

## Image

```javascript
import MultiLoader from '@thibka/three-multiloader'

let loader = new MultiLoader()

loader.onLoaded.add(() => {
    console.log(loader.images['dog'])
})

loader.load([
    { alias: 'dog', type: 'image', path: 'path/to/image.jpg' }    
])
```

## GLTF

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

let loader = new MultiLoader({ GLTFLoader })

loader.onLoaded.add(() => {
    console.log(loader.models['main-scene'])
})

loader.load([
    { alias: 'main-scene', type: 'gltf', path: 'path/to/model.glb' }
])
```

## Video

```javascript
import MultiLoader from '@thibka/three-multiloader'

let loader = new MultiLoader()

loader.onLoaded.add(() => {
    console.log(loader.videos['my-video'])
})

loader.load([
    // The 'loadEvent' property can be any video event ('loadedmetadata', 'canplaythrough', etc.), 
    // defining when the video is considered loaded.
    // Default is 'loadedmetadata'.
    { alias: 'my-video', type: 'video', path: './path/to/video.mp4', loadEvent: 'loadedmetadata' },
])
```


## CubeTexture

The `alias` attribute must be provided for each cube-texture, since they don't have a single unique path.

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { CubeTextureLoader } from 'three';

const loader = new MultiLoader({ CubeTextureLoader });

loader.onLoaded.add(() => {
    console.log(loader.cubeTextures['skybox']);
});

loader.load([
    { alias: 'skybox', type: 'cube-texture', path: [
        'path/to/px.png', 'path/to/nx.png',
        'path/to/py.png', 'path/to/ny.png',
        'path/to/pz.png', 'path/to/nz.png'
    ]}
]);
```

## KTX2 compressed texture
Transcoder files must be provided in the build.  
This can be done in various ways:
#### Manually
By moving the file from `node_modules/three/examples/jsm/libs/basis/` to `build/basis/` 

#### With Webpack
By using copy-webpack-plugin:
```javascript
/* webpack.config.js */
const CopyPlugin = require('copy-webpack-plugin');
let config = {
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: 'node_modules/three/examples/jsm/libs/basis', to: 'basis' },
            ]
        })
    ]
}
```

#### With Vite
Using the vite-plugin-static-copy plugin:
```javascript
/* vite.config.js */
import { viteStaticCopy } from 'vite-plugin-static-copy';
export default {
    plugins: [
        viteStaticCopy({
            targets: [
                {
                    src: 'node_modules/three/examples/jsm/libs/basis',
                    dest: ''
                }
            ]
        })
    ]
}
```

You can then use it this way:

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { KTX2Loader } from 'three/examples/jsm/loaders/KTX2Loader.js';

const loader = new MultiLoader({ 
    KTX2Loader, 
    basisTranscoderPath: 'basis/', // Transcoder files must be provided in the build. See above.
    renderer // The renderer must be provided so that the basisLoader can work.
});

loader.onLoaded.add( callbackFunction );

loader.load([
    { alias: 'some-texture', type: 'texture', path: 'path/to/my_texture.ktx2' }
]);

function callbackFunction() {
    console.log(loader.textures['some-texture']);
}
```

## DRACO compressed GLTF file
draco_decoder.js (~600ko) must be provided in the build.  
This can be done in various ways:

#### Manually
By moving the file from `node_modules/three/examples/jsm/libs/draco/draco_decoder.js` to `build/draco/draco_decoder.js` 

#### With Webpack
Using copy-webpack-plugin:
```javascript
/* webpack.config.js */
const CopyPlugin = require('copy-webpack-plugin');
let config = {
    plugins: [
        new CopyPlugin({
            patterns: [
                { from: 'node_modules/three/examples/jsm/libs/draco/draco_decoder.js', to: 'draco/draco_decoder.js' },
            ]
        })
    ]
}
```

#### With Vite
Using the vite-plugin-static-copy plugin:
```javascript
/* vite.config.js */
import { viteStaticCopy } from 'vite-plugin-static-copy';
export default {
    plugins: [
        viteStaticCopy({
            targets: [
                {
                    src: 'node_modules/three/examples/jsm/libs/draco/draco_decoder.js',
                    dest: 'draco'
                }
            ]
        })
    ]
}
```

Then use it this way:

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';

const loader = new MultiLoader({ 
    DRACOLoader,
    DRACODecoderPath: 'draco/' // draco_decoder.js must be provided in the build.
});

loader.onLoaded.add( callbackFunction );

loader.load([
    { alias: 'some-model', type: 'gltf-draco', path: 'path/to/model.glb' }
]);

function callbackFunction() {
    let gltf = loader.models['some-model'];

    scene.add(gltf.scene);
}
```

## .drc file
This loading method should only be used for `.drc` files.
```javascript
import MultiLoader from '@thibka/three-multiloader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js';

const loader = new MultiLoader({ 
    DRACOLoader
});

loader.onLoaded.add( callbackFunction );

loader.load([
    { alias: 'some-model', type: 'drc', path: 'path/to/model.drc' }
]);

function callbackFunction() {
    let geometry = loader.models['some-model'];
    let material = new THREE.MeshStandardMaterial();
    let dracoMesh = new THREE.Mesh( geometry, material );
    scene.add( dracoMesh );
}
```

## Lottie texture

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { LottieLoader } from 'three/examples/jsm/loaders/LottieLoader.js';

const loader = new MultiLoader({ 
    lottieLoader: LottieLoader,
    lottieLoaderQuality: 2 // default is 1
});

loader.onLoaded.add(() => {
    // Once it's loaded as a texture, access the lottie animation with texture.animation
    console.log(loader.textures['lottie'].animation);
});

loader.load([
    { alias: 'lottie', type: 'texture', path: 'textures/lottie-logo-animation.json' }
]);
```

## OBJ

```javascript
import MultiLoader from '@thibka/three-multiloader'
import { OBJLoader } from 'three/examples/jsm/loaders/GLTFLoader';

let loader = new MultiLoader({ OBJLoader })

loader.onLoaded.add(() => {
    console.log(loader.models['objModel'])
})

loader.load([
    { alias: 'objModel', type: 'obj', path: 'path/to/model.obj' }
])
```